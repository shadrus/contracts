package controllers;

import models.papki;
import play.data.format.Formats;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 12.03.13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
public class EditDocClass {


    public String contract_num;

    public String contract_name;

    public String contract_bigdeal;

    @Formats.DateTime(pattern="dd.MM.yyyy")
    public Date contract_date_start;

    @Formats.DateTime(pattern="dd.MM.yyyy")
    public Date contract_date_end;

    public BigDecimal contract_price;

    public boolean contract_prolong;



}
