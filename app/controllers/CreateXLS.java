package controllers;
import  jxl.*;
import jxl.format.*;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.write.*;
import jxl.write.Number;
import models.contracts;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.math.BigDecimal;


/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 28.03.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
public class CreateXLS {
     public void get(List<contracts> contr) throws IOException {
         // Create a cell format for Arial 10 point font
         WritableFont times11font = new WritableFont(WritableFont.TIMES, 11);
         WritableFont times11usual = new WritableFont(WritableFont.TIMES, 11);
         DateFormat customDateFormat = new DateFormat ("dd.MMM.yyyy");
         WritableCellFormat dateFormat = new WritableCellFormat (customDateFormat);
         try {
             times11font.setBoldStyle(WritableFont.BOLD);

         } catch (WriteException e) {
             e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
         }

         WritableCellFormat times11format = new WritableCellFormat (times11font);
         WritableCellFormat times11usualformat = new WritableCellFormat (times11usual);
         try {
             times11format.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
             times11format.setWrap(true);
             times11format.setAlignment(Alignment.CENTRE);
             times11usualformat.setWrap(true);
             times11usualformat.setAlignment(Alignment.CENTRE);
             times11usualformat.setBorder(Border.ALL, BorderLineStyle.THIN);
             dateFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

         } catch (WriteException e) {
             e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
         }


         FileOutputStream out = new FileOutputStream("../docs/public/report/report.xls");
             WritableWorkbook workbook = Workbook.createWorkbook(out);
             WritableSheet sheet = workbook.createSheet("договора", 0);

             sheet.setColumnView(0, 8);
             sheet.setColumnView(1, 15);
             sheet.setColumnView(2, 15);
             sheet.setColumnView(3, 15);
             sheet.setColumnView(4, 20);
             sheet.setColumnView(5, 30);
             sheet.setColumnView(6, 25);
             sheet.setColumnView(7, 15);
             sheet.setColumnView(8, 20);
             sheet.setColumnView(9, 15);
             sheet.setColumnView(10, 20);
             sheet.setColumnView(11, 20);
             sheet.setColumnView(12, 20);
             Label label1 = new Label(0,0,"№ п/п", times11format);
             Label label2 = new Label(1,0,"НОМЕР ДОГОВОРА", times11format);
             Label label3 = new Label(2,0,"ДАТА ДОГОВОРА", times11format);
             Label label4 = new Label(3,0,"СРОК ДЕЙСТВИЯ ДОГОВОРА", times11format);
             Label label5 = new Label(4,0,"УСЛОВИЕ О ПРОЛОНГАЦИИ", times11format);
             Label label6 = new Label(5,0,"КОНТРАГЕНТ", times11format);
             Label label7 = new Label(6,0,"ПРЕДМЕТ ДОГОВОРА", times11format);
             Label label8 = new Label(7,0,"ЦЕНА ПО ДОГОВОРУ", times11format);
             Label label9 = new Label(8,0,"КРУПНАЯ СДЕЛКА/ СДЕЛКА С ЗАИНТЕРЕСОВАННОСТЬЮ", times11format);
             Label label10 = new Label(9,0,"НАЛИЧИЕ ДОПОЛНИТЕЛЬНОГО СОГЛАШЕНИЯ", times11format);
             Label label11 = new Label(10,0,"ГДЕ ХРАНИТСЯ", times11format);
             Label label12 = new Label(11,0,"ПРИМЕЧАНИЯ", times11format);
             Label label13 = new Label(12,0,"ИНИЦИАТОР", times11format);
            // Label label14 = new Label(13,0,"ИСПОЛНИТЕЛЬ", times11format);
             int a=0;
             String prolong=null;
             String protocol=null;
             while(a<contr.size()){

                 if (contr.get(a).contract_bigdeal==null || contr.get(a).contract_bigdeal=="" || contr.get(a).contract_bigdeal.isEmpty()){
                     protocol="нет";
                 }
                 else{
                     protocol=contr.get(a).contract_bigdeal;
                 }

             Number labelsub1 = new Number(0,a+1, a+1, times11usualformat);
             Label labelsub2 = new Label(1,a+1, contr.get(a).contract_num, times11usualformat);
             DateTime labelsub3 = new DateTime(2,a+1, contr.get(a).contract_date_start, dateFormat);
             if (contr.get(a).contract_date_end!=null){
             DateTime labelsub4 = new DateTime(3,a+1, contr.get(a).contract_date_end, dateFormat);
                 try {
                     sheet.addCell(labelsub4);
                 } catch (WriteException e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                 }

             } else{
                Label labelsub4 = new Label(3, a+1, "", times11usualformat);
                 try {
                     sheet.addCell(labelsub4);
                 } catch (WriteException e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                 }
             }



                 if (contr.get(a).contract_prolong==true){
                     prolong="да";
                 }
                 else{
                     prolong="нет";
                 }

             Label labelsub5 = new Label(4,a+1, prolong, times11usualformat);
             Label labelsub6 = new Label(5,a+1, contr.get(a).contr_c.contr_name, times11usualformat);
             Label labelsub7 = new Label(6,a+1, contr.get(a).contract_name, times11usualformat);

             if (contr.get(a).contract_price==null){
                  Label labelsub8 = new Label(7,a+1, "согласно счета", times11usualformat);
                 try {
                     sheet.addCell(labelsub8);
                 } catch (WriteException e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                 }
             }  else{
                 Number labelsub8 = new Number(7,a+1, contr.get(a).contract_price.doubleValue(),times11usualformat);
                 try {
                     sheet.addCell(labelsub8);
                 } catch (WriteException e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                 }
             }

             Label labelsub9 = new Label(8, a+1, protocol, times11usualformat);
             String dopnik=null;
             if (contr.get(a).dop_c==null){
                 dopnik="нет";
             }
                 else{
                 dopnik="да";
             }
             Label labelsub10 = new Label(9, a+1, dopnik, times11usualformat);
             Label labelsub11 = new Label(10, a+1, contr.get(a).papki_c.papki_name, times11usualformat);
             Label labelsub12 = new Label(11, a+1, contr.get(a).contract_story, times11usualformat);
             Label labelsub13 = new Label(12, a+1, contr.get(a).init_c.user_fio, times11usualformat);

                 a++;

                 try {
                     sheet.addCell(labelsub1);
                     sheet.addCell(labelsub2);
                     sheet.addCell(labelsub3);
                     sheet.addCell(labelsub5);
                     sheet.addCell(labelsub6);
                     sheet.addCell(labelsub7);
                     sheet.addCell(labelsub9);
                     sheet.addCell(labelsub10);
                     sheet.addCell(labelsub11);
                     sheet.addCell(labelsub12);
                     sheet.addCell(labelsub13);
                 } catch (WriteException e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                 }
             }
         try {
             sheet.addCell(label1);
             sheet.addCell(label2);
             sheet.addCell(label3);
             sheet.addCell(label4);
             sheet.addCell(label5);
             sheet.addCell(label6);
             sheet.addCell(label7);
             sheet.addCell(label8);
             sheet.addCell(label9);
             sheet.addCell(label10);
             sheet.addCell(label11);
             sheet.addCell(label12);
             sheet.addCell(label13);
            // sheet.addCell(label14);
         } catch (WriteException e) {
             e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
         }
         try {
         workbook.write();
         workbook.close();
         } catch (WriteException e) {
             e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
         }






     }
}
