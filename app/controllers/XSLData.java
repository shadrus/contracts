package controllers;

import play.data.format.Formats;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 28.03.13
 * Time: 15:26
 * To change this template use File | Settings | File Templates.
 */
public class XSLData {


        @Formats.DateTime(pattern="dd.MM.yyyy")
        public Date contract_date_start;

        @Formats.DateTime(pattern="dd.MM.yyyy")
        public Date contract_date_end;

        @Formats.DateTime(pattern="dd.MM.yyyy")
        public Date contract_ends;
}
