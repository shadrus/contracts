package controllers;

import java.io.*;
import java.net.URLEncoder;

import java.security.NoSuchAlgorithmException;
import java.util.*;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import play.Logger;
import play.Routes;
import play.mvc.*;
import play.data.*;
import org.apache.commons.io.IOUtils;

import static play.data.Form.*;

import play.mvc.Http.MultipartFormData;

import views.html.*;

import models.*;

import java.security.MessageDigest;

public class Application extends Controller {

    public static Result index(int p) {


        return ok(index.render(p));
    }

    public static Result more() {

        return ok(more.render());
    }

    public static Result register() {
        Form<user> userForm = form(user.class).bindFromRequest();
        if (userForm.hasErrors()) {
            return badRequest(register.render(userForm));
        }
        userForm.get().save();
        flash("success", "Пользователь " + userForm.get().user_name + " создан");
        return redirect(routes.Application.index(1));
    }

    public static Result login() throws NoSuchAlgorithmException {
        //записываем данные из реквеста
        MessageDigest md = MessageDigest.getInstance("SHA-256");


        Form<UserLogin> loginForm = form(UserLogin.class);
        UserLogin userBind = loginForm.bindFromRequest().get();

        md.update(userBind.user_pass.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        userBind.user_pass = sb.toString();

        //делаем запрос в таблицу user
        Query<user> query = Ebean.createQuery(user.class);
        //ограничивает запрос колонками user_name, user_pass
        query.select("user_name, user_pass, is_Active");
        //устанавливаем значения для where
        query.where("user_name=:user_name and user_pass=:user_pass");
        query.setParameter("user_name", userBind.user_name);
        query.setParameter("user_pass", userBind.user_pass);
        user curuser = query.findUnique();


        if (curuser == null) {
            return redirect(routes.Application.index(2));
        }

        //если админ
        else if (curuser.status_u.status_id == 1 && curuser.is_Active=='A') {
            session().clear();
            session("loged", curuser.user_id.toString());
            session("status", curuser.status_u.status_id.toString());
            Logger.ALogger log = play.Logger.of("application");
            log.info("Logged " + user.find.byId(Integer.parseInt(session("loged"))).user_name);
            return redirect(routes.Application.AddOtdelPage(0));
        }
        //если юрист
        else if (curuser.status_u.status_id == 2 && curuser.is_Active=='A') {
            session().clear();
            session("loged", curuser.user_id.toString());
            session("status", curuser.status_u.status_id.toString());
            Logger.ALogger log = play.Logger.of("application");
            log.info("Logged " + user.find.byId(Integer.parseInt(session("loged"))).user_name);
            return redirect(routes.Application.SearchDoc(0));
        }
        //если пользователь
        else if (curuser.status_u.status_id == 3 && curuser.is_Active=='A') {
            session().clear();
            session("loged", curuser.user_id.toString());
            session("status", curuser.status_u.status_id.toString());
            Logger.ALogger log = play.Logger.of("application");
            log.info("Logged " + user.find.byId(Integer.parseInt(session("loged"))).user_name);
            return redirect(routes.Application.SearchDocUser(0));
        }
        //если ограниченный пользователь
        else if (curuser.status_u.status_id == 4 && curuser.is_Active=='A') {
            session().clear();
            session("loged", curuser.user_id.toString());
            session("status", curuser.status_u.status_id.toString());
            Logger.ALogger log = play.Logger.of("application");
            log.info("Logged " + user.find.byId(Integer.parseInt(session("loged"))).user_name);
            return redirect(routes.Application.SearchDocUser(0));
        } else {
            return redirect(routes.Application.index(1));
        }

    }


    public static Result logout() {
        session().clear();
        return redirect(routes.Application.index(1));
    }


    //Добавляем отдел
    public static Result AddOtdel() {
        Form<otdel> addOtdel = form(otdel.class);
        otdel newotdel = addOtdel.bindFromRequest().get();
        newotdel.save();
        return ok(addotdel.render(otdel.page(0)));
    }

    public static Result AddOtdelPage(int p) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(1);
        if (test == true) {
            return ok(addotdel.render(otdel.page(p)));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    //удаляем отдел
    public static Result DelOtdel(int id) {
        otdel.find.ref(id).delete();
        Logger.ALogger log = play.Logger.of("application");
        log.info("department deleted by " + user.find.byId(Integer.parseInt(session("loged"))).user_name);
        return ok(addotdel.render(otdel.page(0)));
    }

    public static Result EditOtdelPage(int id) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(1);
        if (test == true) {
            return ok(editotdel.render(id));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result EditOtdel() {
        Form<otdel> addOtdel = form(otdel.class);
        otdel newotdel = addOtdel.bindFromRequest().get();
        newotdel.update();
        return ok(addotdel.render(otdel.page(0)));
    }

    //рабоаем с должностями
    public static Result AddDolg() {
        Form<dolg> addDolg = form(dolg.class);
        dolg newdolg = addDolg.bindFromRequest().get();
        System.out.println(newdolg.dolg_id);
        newdolg.save();
        return ok(adddolg.render(dolg.page(0), otdel.all()));
    }


    public static Result AddDolgPage(int p) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(1);
        if (test == true) {
            return ok(adddolg.render(dolg.page(p), otdel.all()));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    //удаляем должность
    public static Result DelDolg(int id) {
        dolg.find.ref(id).delete();
        return ok(adddolg.render(dolg.page(0), otdel.all()));
    }

    public static Result EditDolgPage(int id) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(1);
        if (test == true) {
            return ok(editdolg.render(id));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result EditDolg() {
        Form<dolg> addDolg = form(dolg.class);
        dolg newdolg = addDolg.bindFromRequest().get();
        newdolg.update();
        return ok(adddolg.render(dolg.page(0), otdel.all()));
    }

    public static Result EditStory(int id) {
        Form<paramSearch> userSearchForm = form(paramSearch.class);
        paramSearch newsearch = userSearchForm.bindFromRequest().get();
        contracts contr = Ebean.find(contracts.class, id);
        contr.contract_story=newsearch.param;
        contr.update();
        return ok(viewdoc.render(id, dopdocs.all(id)));
    }


    public static Result AddUserPage(int p) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(1);
        if (test == true) {
            return ok(adduser.render(user.page(p), otdel.all(), dolg.all(), status.all()));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result AddUser() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        Form<user> addUser = form(user.class);
        user newuser = addUser.bindFromRequest().get();
        md.update(newuser.user_pass.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        newuser.user_pass = sb.toString();
        newuser.is_Active='A';
        newuser.save();
        return ok(adduser.render(user.page(0), otdel.all(), dolg.all(), status.all()));
    }

    public static Result DelUser(int id) {
        Logger.ALogger log = play.Logger.of("application");
        log.info("user deleted by " + user.find.byId(Integer.parseInt(session("loged"))).user_name);
        user edit = Ebean.find(user.class, id);
        if (user.find.byId(id).is_Active=='I'){
        edit.is_Active='A';
        }
        else{
            edit.is_Active='I';
        }
        edit.update();
        //user.find.ref(id).update("is_Active I");
        //return ok(adduser.render(user.page(0), otdel.all(), dolg.all(), status.all()));
        return redirect(routes.Application.AddUserPage(0));
    }

    public static Result EditUserPage(int id) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(1);
        if (test == true) {
            return ok(edituser.render(id, status.all(), dolg.all()));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result EditUser() throws NoSuchAlgorithmException {
        Form<user> editUser = form(user.class);
        user newuser = editUser.bindFromRequest().get();
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(newuser.user_pass.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        newuser.user_pass = sb.toString();
        newuser.is_Active=Ebean.find(user.class).setId(newuser.user_id).findUnique().is_Active;
        newuser.update();
        return ok(adduser.render(user.page(0), otdel.all(), dolg.all(), status.all()));
    }

    public static Result searchUser() {
        Form<paramSearch> userSearchForm = form(paramSearch.class);
        paramSearch newsearch = userSearchForm.bindFromRequest().get();
        return ok(adduser.render(user.pageSearch(0, newsearch), otdel.all(), dolg.all(), status.all()));
        //TODO поиск поломался
        //return ok(adduser.render(res, otdel.all(), dolg.all(), status.all()));

    }

    public static Result searchOtdel() {
        Form<paramSearch> userSearchForm = form(paramSearch.class);
        paramSearch newsearch = userSearchForm.bindFromRequest().get();
        return ok(addotdel.render(otdel.pageSearch(0, newsearch)));


    }


    public static Result AddContactPage(int p) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
            return ok(addcontact.render(contacts.page(p)));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result AddContact(int id) {
        Form<contacts> contactsForm = form(contacts.class);
        contacts edit_c = contactsForm.bindFromRequest().get();
        contracts contr = Ebean.find(contracts.class, id);
        contr.contacts_c=edit_c;
        contr.update();
        return ok(viewdoc.render(id, dopdocs.all(id)));

    }

    public static Result DelContact(int id) {
        contacts.find.ref(id).delete();
        Logger.ALogger log = play.Logger.of("application");
        log.info("contact deleted by " + user.find.byId(Integer.parseInt(session("loged"))).user_name);
        return ok(addcontact.render(contacts.page(0)));
    }

    public static Result EditContactPage(int id) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
            return ok(editcontact.render(id));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result EditContact() {
        Form<contacts> editUser = form(contacts.class);
        contacts newcontr = editUser.bindFromRequest().get();

        newcontr.update();
        return ok(addcontact.render(contacts.page(0)));
    }

    public static Result searchContact() {
        Form<paramSearch> contactSearchForm = form(paramSearch.class);
        paramSearch newsearch = contactSearchForm.bindFromRequest().get();
        return ok(addcontact.render(contacts.pageSearch(0, newsearch)));


    }

    public static Result AddContragentPage(int p) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
            return ok(addcontragent.render(contragents.page(p)));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result AddContragent(int page) {
        Form<contragents> addContragents = form(contragents.class);
        contragents newcontr = addContragents.bindFromRequest().get();
        newcontr.save();
        if (page == 1) {
            return ok(newcontr.contr_id.toString());
        } else {
            return ok(addcontragent.render(contragents.page(0)));
        }
    }

    public static Result EditContragentPage(int id) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
            return ok(editcontragent.render(id));
        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result EditContragent() {
        Form<contragents> editUser = form(contragents.class);
        contragents newcontr = editUser.bindFromRequest().get();

        newcontr.update();
        return ok(addcontragent.render(contragents.page(0)));
    }

    public static Result searchContragent() {
        Form<paramSearch> contrSearchForm = form(paramSearch.class);
        paramSearch newsearch = contrSearchForm.bindFromRequest().get();
        return ok(addcontragent.render(contragents.pageSearch(0, newsearch)));

    }

    public static Result searchDolg() {
        Form<paramSearch> dolgSearchForm = form(paramSearch.class);
        paramSearch newsearch = dolgSearchForm.bindFromRequest().get();
        return ok(adddolg.render(dolg.pageSearch(0, newsearch), otdel.all()));

    }


    public static Result AddDoc() {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
        MultipartFormData body = request().body().asMultipartFormData();

        MultipartFormData.FilePart picture = body.getFile("scanname");
        MultipartFormData.FilePart docfile = body.getFile("doc");

        //Добавляю текстовый вариант договора

        if (picture != null) {
            String fileName = picture.getFilename();
            int dotPos = fileName.lastIndexOf(".") + 1;

            String ext = fileName.substring(dotPos);
            if (ext.equals("pdf")) {
                String contentType = picture.getContentType();
                File file = picture.getFile();
                String hashfilename = UUID.randomUUID().toString();

                file.renameTo(new File("../docs/public/uploads/images/" + hashfilename));

                Form<contracts> addContractForm = form(contracts.class);
                contracts newcontract = new contracts();
                newcontract = addContractForm.bindFromRequest().get();
                Integer urist_id = Integer.parseInt(session("loged"));
                newcontract.contract_urist = urist_id;
                newcontract.uidname = hashfilename;
                newcontract.contract_txt_name = fileName;


                if (docfile != null) {
                    String origName = docfile.getFilename();
                    int dotPos2 = origName.lastIndexOf(".") + 1;
                    String ext2 = origName.substring(dotPos2);
                    if ((ext2.equals("doc")) || (ext2.equals("docx")) || (ext2.equals("odt"))) {
                        String origcontentType = docfile.getContentType();
                        File origfile = docfile.getFile();
                        String hashorigfilename = UUID.randomUUID().toString();
                        origfile.renameTo(new File("../docs/public/uploads/docs/" + hashorigfilename));
                        newcontract.orig_uid_name = hashorigfilename;
                        newcontract.orig_txt_name = origName;

                    }


                }

                    newcontract.save();



            } else {
                routes.Application.AddContractPage();
            }


        }
        return ok(searchdoc.render(contracts.page(0), papki.all()));
        }
        else{
            return redirect(routes.Application.index(1));
        }
    }

    public static Result EditDoc(int id) {
        Form<EditDocClass> contractsForm = form(EditDocClass.class);
        EditDocClass editDoc = contractsForm.bindFromRequest().get();
        contracts contr = Ebean.find(contracts.class, id);
        contr.contract_name=editDoc.contract_name;
        contr.contract_num=editDoc.contract_num;
        contr.contract_price=editDoc.contract_price;
        contr.contract_prolong=editDoc.contract_prolong;
        contr.contract_date_start=editDoc.contract_date_start;
        contr.contract_date_end=editDoc.contract_date_end;
        contr.contract_bigdeal=editDoc.contract_bigdeal;
        contr.update();
        return ok(viewdoc.render(id, dopdocs.all(id)));
    }


    public static Result SearchDoc(int page) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
            return ok(searchdoc.render(contracts.page(page), papki.all()));
        } else {
            return redirect(routes.Application.index(1));
        }
    }

    public static Result SearchDocUser(int page) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(3);
        if (test == true) {
            return ok(searchdocuser.render(contracts.page(page), papki.all()));
        }  else
         {
            test=auth.test(4);
             if (test == true) {

                 return ok(searchdocuser.render(contracts.pageOgran(page, user.find.byId(Integer.parseInt(session("loged"))).dolg_u.otdel_d.otdel_id), papki.all()));
             }
             else{
            return redirect(routes.Application.index(1));
             }
        }
    }

    public static Result FullSearchDoc() {   //user==2 - юрист, а 3-пользователь
        Form<DocSearch> SearchForm = form(DocSearch.class);
        DocSearch newsearch = SearchForm.bindFromRequest().get();
        String user = session("status");
        if (user.equals("2")) {
            return ok(searchdoc.render(contracts.searchpage(0, newsearch), papki.all()));
        } else if (user.equals("3")) {
            return ok(searchdocuser.render(contracts.searchpage(0, newsearch), papki.all()));
        } else {
            return redirect(routes.Application.index(1));
        }

    }


    public static Result AddContractPage() {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
            Query<contragents> query = Ebean.createQuery(contragents.class);
            Query<user> query1 = Ebean.createQuery(user.class);
            //ограничивает запрос колонками user_name, user_pass
            query.select("contr_name");
            query1.select("user_name");
            //устанавливаем значения для where
            //["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Wyoming"]
            List<contragents> res = query.findList();
            List<user> res1 = query1.findList();
            String[] final_res = new String[2];
            if (res != null) {

                for (int a = 0; a < 2; a++) {
                    int iter = 0;
                    if (a == 0) {
                        iter = res.size();
                    } else if (a == 1) {
                        iter = res1.size();
                    }


                    final_res[a] = "[";

                    for (int i = 0; i < iter; i++) {
                        //res.get(i).contr_name.toString().replace("\"","\'")
                        if (a == 0) {
                            final_res[a] = final_res[a] + '\"' + res.get(i).contr_name.toString().replace("\"", "\'") + '\"' + ',';
                        } else if (a == 1) {
                            final_res[a] = final_res[a] + '\"' + res1.get(i).user_fio.toString().replace("\"", "\'") + '\"' + ',';
                        }


                    }
                    final_res[a] = final_res[a].substring(0, final_res[a].length() - 1);
                    final_res[a] = final_res[a] + "]";
                }
            }


            return ok(addcontract.render(user.all(), contragents.all(), papki.all()));

        } else {
            return redirect(routes.Application.index(1));
        }


    }

    public static Result AddDopPage(int id) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {

            return ok(adddop.render(id));

        } else {
            return redirect(routes.Application.index(1));
        }


    }

    public static Result AddDop(int id) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
            MultipartFormData body = request().body().asMultipartFormData();

            MultipartFormData.FilePart picture = body.getFile("dop_scanname");
            MultipartFormData.FilePart docfile = body.getFile("doc");

            //Добавляю текстовый вариант договора

            if (picture != null) {
                String fileName = picture.getFilename();
                int dotPos = fileName.lastIndexOf(".") + 1;

                String ext = fileName.substring(dotPos);
                if (ext.equals("pdf")) {
                    String contentType = picture.getContentType();
                    File file = picture.getFile();
                    String hashfilename = UUID.randomUUID().toString();

                    file.renameTo(new File("../docs/public/uploads/images/" + hashfilename));

                    Form<dopdocs> addContractForm = form(dopdocs.class);
                    dopdocs newcontract = new dopdocs();
                    newcontract = addContractForm.bindFromRequest().get();
                    Integer urist_id = Integer.parseInt(session("loged"));
                    newcontract.dop_urist = urist_id;
                    newcontract.dop_uid_scanname = hashfilename;
                    newcontract.dop_scanname = fileName;


                    if (docfile != null) {
                        String origName = docfile.getFilename();
                        int dotPos2 = origName.lastIndexOf(".") + 1;
                        String ext2 = origName.substring(dotPos2);
                        if ((ext2.equals("doc")) || (ext2.equals("docx")) || (ext2.equals("odt"))) {
                            String origcontentType = docfile.getContentType();
                            File origfile = docfile.getFile();
                            String hashorigfilename = UUID.randomUUID().toString();
                            origfile.renameTo(new File("../docs/public/uploads/docs/" + hashorigfilename));
                            newcontract.uidname = hashorigfilename;
                            newcontract.dop_orig_name = origName;

                        }


                    }

                    newcontract.save();

                } else {
                    routes.Application.AddDopPage(id);
                }
            }
        } else {
            return redirect(routes.Application.index(1));

        }
        return ok(viewdoc.render(id, dopdocs.all(id)));

    }


    public static Result FindContrId() {
        Form<paramSearch> userSearchForm = form(paramSearch.class);
        paramSearch newsearch = userSearchForm.bindFromRequest().get();
        System.out.println(newsearch.param.toString());

        String searchword = newsearch.param.toString().replace("\'", "\"");
        System.out.println(searchword);
        Query<contragents> query = Ebean.createQuery(contragents.class);
        //ограничивает запрос колонками user_name, user_pass
        query.select("contr_id");
        //устанавливаем значения для where
        query.where().ilike("contr_name", "%" + searchword + "%");
        String contrid = query.findUnique().contr_id.toString();
        return ok(contrid);

    }

    public static Result DelContract(int id) {
        //TODO добавить удаление файлов
        if (contracts.find.byId(id).uidname != null) {
            String file = "../docs/public/uploads/images/" + contracts.find.byId(id).uidname;
            File f1 = new File(file);
            boolean success = f1.delete();
            if (!success) {
                System.out.println("Deletion doc" + contracts.find.byId(id).uidname + " failed.");

            } else {
                System.out.println("File  doc " + contracts.find.byId(id).uidname + "  deleted.");
            }
        }
        if (contracts.find.byId(id).orig_uid_name != null) {
            String file2 = "../docs/public/uploads/docs/" + contracts.find.byId(id).orig_uid_name;
            File f2 = new File(file2);
            boolean success = f2.delete();

            if (!success) {
                System.out.println("Deletion failed.");

            } else {
                System.out.println("File doc " + contracts.find.byId(id).orig_uid_name + " deleted.");
            }
        }

        //удаляем допники
        List<dopdocs> del_dops = dopdocs.searchDopsOrig(id);
        for (int i = 0; i < del_dops.size(); i++) {

            String file = "../docs/public/uploads/docs/" + del_dops.get(i).uidname;
            File f1 = new File(file);
            try {
                System.out.println(f1.getCanonicalPath().toString());
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            boolean success = f1.delete();
            if (!success) {
                System.out.println("Deletion dop " + del_dops.get(i).uidname + " failed.");

            } else {
                System.out.println("File dop " + del_dops.get(i).uidname + " deleted.");
            }
        }
        del_dops = dopdocs.searchDopsScan(id);
        for (int i = 0; i < del_dops.size(); i++) {
            String file = "../docs/public/uploads/images/" + del_dops.get(i).dop_uid_scanname;
            File f1 = new File(file);
            try {
                System.out.println(f1.getCanonicalPath().toString());
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            boolean success = f1.delete();
            if (!success) {
                System.out.println("Deletion dop" + del_dops.get(i).dop_uid_scanname + " failed.");

            } else {
                System.out.println("File dop " + del_dops.get(i).dop_uid_scanname + " deleted.");
            }
        }
        //contracts.find.byId(id).contacts_c.delete();
        contracts.find.byId(id).delete();
        Logger.ALogger log = play.Logger.of("application");
        log.info("contract deleted by " + user.find.byId(Integer.parseInt(session("loged"))).user_name);
        return ok(searchdoc.render(contracts.page(0), papki.all()));
    }

    public static Result GetFile(int id, int scan_id, boolean scan, boolean dopdoc) throws FileNotFoundException {
        String orig;
        String hash;

        if (scan == true) {
            response().setContentType("application/pdf");
            //если договор
            if (dopdoc == false) {
                hash = contracts.find.byId(id).uidname;
                orig = contracts.find.byId(id).contract_txt_name;
            }
            //если доп соглашение
            else {
                hash = contracts.find.byId(id).dop_c.get(scan_id).dop_uid_scanname;
                orig = contracts.find.byId(id).dop_c.get(scan_id).dop_scanname;
            }

            try {
                orig = URLEncoder.encode(orig, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            File file = new File("../docs/public/uploads/images/" + hash);
            response().setHeader("Content-Disposition", "attachment; filename*=\"utf-8'" + orig + "\"");
            response().setHeader("Content-Length", String.valueOf(file.length()));

            return ok(new FileInputStream(file));
        } else {
            response().setContentType("application/ms-word");
            if (dopdoc == false) {
                hash = contracts.find.byId(id).orig_uid_name;
                orig = contracts.find.byId(id).orig_txt_name;
            } else {
                hash = contracts.find.byId(id).dop_c.get(scan_id).uidname;
                orig = contracts.find.byId(id).dop_c.get(scan_id).dop_orig_name;
            }
            try {
                orig = URLEncoder.encode(orig, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            File file = new File("../docs/public/uploads/docs/" + hash);
            response().setHeader("Content-Disposition", "attachment; filename*=\"utf-8'" + orig + "\"");
            response().setHeader("Content-Length", String.valueOf(file.length()));

            return ok(new FileInputStream(file));
        }


    }

    public static Result ViewDocPage(int id) {
        boolean test;
        authtest auth = new authtest();
        test = auth.test(2);
        if (test == true) {
            return ok(viewdoc.render(id, dopdocs.all(id)));

        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result ViewDocUserPage(int id) {
        boolean test;
        boolean test2;
        authtest auth = new authtest();
        test = auth.test(3);
        test2 = auth.test(4);
        if ((test == true) || (test2 == true)) {
            return ok(viewdocuser.render(id, dopdocs.all(id)));

        } else {
            return redirect(routes.Application.index(1));
        }

    }

    public static Result AddFolderPage(int page) {
        return ok(addfolder.render(papki.page(page)));

    }

    public static Result EditFolderPage(int id) {
        return ok(editfolder.render(id));

    }


    public static Result EditFolder() {
        Form<papki> editFolder = form(papki.class);
        papki newpapka = editFolder.bindFromRequest().get();
        newpapka.update();
        return ok(addfolder.render(papki.page(0)));
    }

    public static Result AddFolder() {
        Form<papki> addForm = form(papki.class);
        papki newPapka = addForm.bindFromRequest().get();
        newPapka.save();
        return ok(addfolder.render(papki.page(0)));
    }

    public static Result ViewPapka(int page, int papki_id) {
        return ok(viewpapka.render(contracts.pagePapki(page, papki_id)));
    }

    public static Result ReportsPage() {

        return ok(reports.render());

    }

    public static Result GetXLS() throws IOException {
        response().setContentType("application/vnd.ms-excel");
        response().setHeader("Content-Disposition", "attachment; filename*=\"utf-8'" + "report.xls" + "\"");
        Form<XSLData> getForm = form(XSLData.class);
        XSLData xslinput = getForm.bindFromRequest().get();
        List <contracts> xsldata = contracts.GetListXLS(xslinput);
        for (int i = 0; i<xsldata.size(); i++){
           System.out.println(xsldata.get(i).contract_num);
        }
        CreateXLS out = new CreateXLS();
        out.get(xsldata);
        return ok(new File("../docs/public/report/report.xls"));
    }


    //JavaScript routes
    public static Result javascriptRoutes() {
        response().setContentType("text/javascript");
        return ok(
                Routes.javascriptRouter("myJsRoutes",
                        routes.javascript.Application.AddContact(),
                        routes.javascript.Application.AddContragent(),
                        routes.javascript.Application.FindContrId())

        );
    }


}
