package models;
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 15.03.13
 * Time: 8:43
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class dopdocs extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer dop_id;

    @Constraints.MaxLength(40)
    @Constraints.Required
    public String contract_num;

    @Formats.DateTime(pattern="dd.MM.yyyy")
    public Date contract_date_start;


    @ManyToOne
    @JoinColumn(name="contract_id",referencedColumnName="contract_id")
    @Constraints.Required
    public contracts contract_d;

    //текстовые версии
    public String dop_orig_name;

    public String uidname;

    //сканы

    public String dop_scanname;

    public String dop_uid_scanname;

    public Integer dop_urist;

    public static Finder<Integer,dopdocs> find = new Finder<Integer, dopdocs>(Integer.class, dopdocs.class);


    public static List <dopdocs> all (int id){
        return find.where()
                .eq("contract_d.contract_id", id)
                .findList();
    }

    public static List<dopdocs> searchDopsOrig(int id) {
        return find.select("uidname")
                .where()
                .eq("contract_id", id)
                .findList();



    }
    public static List<dopdocs> searchDopsScan(int id) {
        return find.select("dop_uid_scanname")
                .where()
                .eq("contract_id", id)
                .findList();
    }



}
