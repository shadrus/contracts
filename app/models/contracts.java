package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 20.02.13
 * Time: 9:02
 * To change this template use File | Settings | File Templates.
 */

import java.text.SimpleDateFormat;
import java.util.*;
import javax.persistence.*;

import controllers.DocSearch;
import controllers.XSLData;
import controllers.paramSearch;
import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;
import java.math.BigDecimal;
 @Entity
public class contracts extends Model {
     @Id
     @GeneratedValue(strategy = GenerationType.SEQUENCE)
     public Integer contract_id;

    @Constraints.Required
     public String contract_num;

     @Constraints.Required
     public String contract_name;

     public String contract_story;

     public String contract_bigdeal;

     @Constraints.Required
     @Formats.DateTime(pattern="dd.MM.yyyy")
     public Date contract_date_start;


     @Formats.DateTime(pattern="dd.MM.yyyy")
     public Date contract_date_end;

     @OneToMany  (mappedBy = "contract_d", cascade = CascadeType.ALL)
     @JoinColumn(name="contract_id",referencedColumnName="contract_id")
     public List <dopdocs> dop_c;


     public String status;

     @ManyToOne
     @JoinColumn(name="init_id",referencedColumnName="user_id")
     @Constraints.Required
     public user init_c;


     public String uidname;

     public BigDecimal contract_price;


     @ManyToOne
     @JoinColumn(name="contr_id",referencedColumnName="contr_id")
     public contragents  contr_c;

     @ManyToOne
     @JoinColumn(name="papki_id",referencedColumnName="papki_id")
     @Constraints.Required
     public papki  papki_c;

     @OneToOne  (optional = false, cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
     @JoinColumn(name="contact_id",referencedColumnName="contact_id")
     public contacts contacts_c;

     public String contract_txt_name;

     public String orig_uid_name;

     public String orig_txt_name;

     public Integer contract_urist;

     public Boolean contract_prolong;


     /**
      * Generic query helper for entity Computer with id Int
      */
     public static Finder<Integer,contracts> find = new Finder<Integer,contracts>(Integer.class, contracts.class);

     public static List <contracts> all (){
         return find.all();
     }

     public static List <contracts> all_desc (){
         return find.order("contract_date_start desc").findList();
     }

     public static Page<contracts> page(int page) {
         return
                 find.where()
                         .orderBy("contract_date_start desc")
                         .findPagingList(15)
                         .getPage(page);
     }

     //для тех, кто может смотреть договор только своего отдела
     public static Page<contracts> pageOgran(int page, int otdel) {
         return
                 find.where()
                         .eq("init_c.dolg_u.otdel_d.otdel_id", otdel)
                         .orderBy("contract_date_start desc")
                          .findPagingList(15)
                         .getPage(page);
     }

     public static Page<contracts> pagePapki(int page, int papka_id) {
         return
                 find.where()
                         .eq("papki_c.papki_id", papka_id)
                         .orderBy("contract_date_start desc")
                         .findPagingList(15)
                         .getPage(page);
     }

     public static ArrayList<List> searchDops(int id) {
         List <contracts> list_orig = find.select("dop_c.uidname")
                                .where()
                                .eq("dop_c.contract_d.contract_id", id)
                                .findList();
         List <contracts> list_scan = find.select("dop_c.dop_uid_scanname")
                 .where()
                 .eq("dop_c.contract_d.contract_id", id)
                 .findList();
         ArrayList <List> list = new ArrayList<List>();
         list.add(0, list_orig);
         list.add(1, list_scan);
         return list;



     }

     public static List <contracts> GetListXLS(XSLData data){
         if (data.contract_date_start==null){
             Date date = new Date(01/01/2000);
             data.contract_date_start=date;
         }
         if(data.contract_date_end==null){
             Date date = new Date();
             SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
             data.contract_date_end=date;
         }
         if (data.contract_ends==null){
             return
                     find.where()
                             .between("contract_date_start", data.contract_date_start, data.contract_date_end)
                             .findList();
         } else{
         return
                 find.where()
                         .le("contract_date_end", data.contract_ends)
                         .between("contract_date_start", data.contract_date_start, data.contract_date_end)
                         .findList();

         }

     }

     public static Page<contracts> searchpage(int page, DocSearch search) {

      if (search.papki_id!=0){
        if (search.contract_ends!=null){
         if ((search.contract_date_start!=null)&&(search.contract_date_end!=null)){
         return
                 find.where()
                         .ilike("contract_num", "%"+search.contract_num+"%")
                         .ilike("contract_name", "%"+search.contract_name+"%")
                         .ilike("contr_c.contr_name", "%"+search.contr_name+"%")
                         .eq("papki_c.papki_id", search.papki_id)
                         .le("contract_date_end", search.contract_ends)

                         .ilike("status", search.status.toString())
                         .between("contract_date_start", search.contract_date_start, search.contract_date_end)
                         .findPagingList(15)
                         .getPage(page);
         }
         else{
             return
             find.where()
                     .ilike("contract_num", "%"+search.contract_num+"%")
                     .ilike("contract_name", "%"+search.contract_name+"%")
                     .ilike("contr_c.contr_name", "%"+search.contr_name+"%")
                      .le("contract_date_end", search.contract_ends)

                     .eq("papki_c.papki_id", search.papki_id)
                     .ilike("status", search.status.toString())
                     .findPagingList(15)
                     .getPage(page);
         }
       }
       else{
          if ((search.contract_date_start!=null)&&(search.contract_date_end!=null)){
              return
                      find.where()
                              .ilike("contract_num", "%"+search.contract_num+"%")
                              .ilike("contract_name", "%"+search.contract_name+"%")
                              .ilike("contr_c.contr_name", "%"+search.contr_name+"%")
                              .eq("papki_c.papki_id", search.papki_id)
                                      //.le("contract_date_end", search.contract_ends)

                              .ilike("status", search.status.toString())
                              .between("contract_date_start", search.contract_date_start, search.contract_date_end)
                              .findPagingList(15)
                              .getPage(page);
          }
          else{
              return
                      find.where()
                              .ilike("contract_num", "%"+search.contract_num+"%")
                              .ilike("contract_name", "%"+search.contract_name+"%")
                              .ilike("contr_c.contr_name", "%"+search.contr_name+"%")
                                      //.le("contract_date_end", search.contract_ends)

                              .eq("papki_c.papki_id", search.papki_id)
                              .ilike("status", search.status.toString())
                              .findPagingList(15)
                              .getPage(page);
          }
      }
      }


        else{
          if (search.contract_ends!=null){

              if ((search.contract_date_start!=null)&&(search.contract_date_end!=null)){
              return
                      find.where()
                              .ilike("contract_num", "%"+search.contract_num+"%")
                              .ilike("contract_name", "%"+search.contract_name+"%")
                              .ilike("contr_c.contr_name", "%"+search.contr_name+"%")
                              .ilike("status", search.status.toString())
                              .le("contract_date_end", search.contract_ends)

                              .between("contract_date_start", search.contract_date_start, search.contract_date_end)
                              .findPagingList(15)
                              .getPage(page);
          }
          else{
              return
                      find.where()
                              .ilike("contract_num", "%"+search.contract_num+"%")
                              .ilike("contract_name", "%"+search.contract_name+"%")
                              .ilike("contr_c.contr_name", "%"+search.contr_name+"%")
                              .le("contract_date_end", search.contract_ends)

                              .ilike("status", search.status.toString())
                              .findPagingList(15)
                              .getPage(page);
          }
      }
          else{
              if ((search.contract_date_start!=null)&&(search.contract_date_end!=null)){
                  return
                          find.where()
                                  .ilike("contract_num", "%"+search.contract_num+"%")
                                  .ilike("contract_name", "%"+search.contract_name+"%")
                                  .ilike("contr_c.contr_name", "%"+search.contr_name+"%")
                                  .ilike("status", search.status.toString())
                                          //.le("contract_date_end", search.contract_ends)

                                  .between("contract_date_start", search.contract_date_start, search.contract_date_end)
                                  .findPagingList(15)
                                  .getPage(page);
              }
              else{
                  return
                          find.where()
                                  .ilike("contract_num", "%"+search.contract_num+"%")
                                  .ilike("contract_name", "%"+search.contract_name+"%")
                                  .ilike("contr_c.contr_name", "%"+search.contr_name+"%")
                                          //.le("contract_date_end", search.contract_ends)

                                  .ilike("status", search.status.toString())
                                  .findPagingList(15)
                                  .getPage(page);
              }
          }
      }

     }

 }
