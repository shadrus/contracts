package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 15.03.13
 * Time: 8:32
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import controllers.paramSearch;
import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;

@Entity
public class papki extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer papki_id;

    @Constraints.Required
    @Constraints.MaxLength(50)
    public String papki_name;

   /* @OneToMany (cascade=CascadeType.ALL, mappedBy="papki_id")
    public List <contracts> contr_p;*/

    public static Finder<Integer,papki> find = new Finder<Integer, papki>(Integer.class, papki.class);


    public static List <papki> all (){
        return find.all();
    }

    public static Page<papki> page(int page) {
        return
                find.where()
                        .findPagingList(15)
                        .getPage(page);
    }

}
