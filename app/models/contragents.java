package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 25.02.13
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import controllers.paramSearch;
import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;
@Entity
public class contragents extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer contr_id;

    @Constraints.Required
    public String contr_name;

    public String contr_country;

    public String contr_state;

    public String contr_town;

    public String contr_street;

    public String contr_inn;

    public String contr_kpp;

    public String contr_bik;

    public String contr_rs;

    public String contr_bank;



    /**
     * Generic query helper for entity Computer with id Int
     */
    public static Finder<Integer,contragents> find = new Finder<Integer, contragents>(Integer.class, contragents.class);

    public static List<contragents> all (){
        return find.all();
    }

    public static Page<contragents> page(int page) {
        return
                find.where()
                        .findPagingList(15)
                        .getPage(page);
    }
    public static Page<contragents> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("contr_name", "%"+newsearch.param+"%")
                        .findPagingList(15)
                        .getPage(page);
    }
}