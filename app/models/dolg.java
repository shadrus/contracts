package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 20.02.13
 * Time: 11:12
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import controllers.paramSearch;
import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;

@Entity
public class dolg extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer dolg_id;

    @Constraints.Required
    public String dolg_name;

    //@Constraints.Required
    @ManyToOne
    @JoinColumn(name="otdel_id",referencedColumnName="otdel_id")
    public otdel otdel_d;

    public static Finder<Integer,dolg> find = new Finder<Integer,dolg>(Integer.class, dolg.class);

    public static List <dolg> all (){
        return find.all();
    }

    public static Page<dolg> page(int page) {
        return
                find.where()
                        .findPagingList(15)
                        .getPage(page);
    }
    public static Page<dolg> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("dolg_name", "%"+newsearch.param+"%")
                        .findPagingList(15)
                        .getPage(page);
    }

    }