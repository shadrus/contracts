package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 25.02.13
 * Time: 9:36
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import controllers.paramSearch;
import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;
@Entity
public class contacts extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer contact_id;

    public String contact_fio;

    public String contact_tel;

    public String contact_mobile;

    @Constraints.Email
    public String contact_email;

    public String contact_dolgn;

    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date contact_birth;


    @OneToOne(mappedBy = "contacts_c")
    public contracts contracts_c;
    //private List<contracts> contractsList;


    /**
     * Generic query helper for entity Computer with id Int
     */
    public static Finder<Integer,contacts> find = new Finder<Integer, contacts>(Integer.class, contacts.class);

    public static List <contacts> all (){
        return find.all();
    }

    public static Page<contacts> page(int page) {
        return
                find.where()
                        .findPagingList(15)
                        .getPage(page);
    }
    public static Page<contacts> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("contracts_c.contr_c.contr_name", "%"+newsearch.param+"%")
                        .findPagingList(15)
                        .getPage(page);
    }
}