package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 19.02.13
 * Time: 13:28
 * To change this template use File | Settings | File Templates.
 */


import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;

//уровень доступа пользователей
@Entity
public class status extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer status_id;

    public String status_name;

    public static Finder<Integer,status> find = new Finder<Integer,status>(Integer.class, status.class);

    public static List<status> all (){
        return find.all();
    }
}
