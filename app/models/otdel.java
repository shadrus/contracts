package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 20.02.13
 * Time: 11:12
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import controllers.paramSearch;
import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;

@Entity
public class otdel extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer otdel_id;

    @Constraints.Required
    public String otdel_name;

       public static Finder<Integer,otdel> find = new Finder<Integer,otdel>(Integer.class, otdel.class);

    public static List <otdel> all (){
        return find.all();
    }

    public static Page<otdel> page(int page) {
        return
                find.where()
                        .findPagingList(20)
                        .getPage(page);
    }

    public static Page<otdel> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("otdel_name", "%"+newsearch.param+"%")
                        .findPagingList(15)
                        .getPage(page);
    }


}
