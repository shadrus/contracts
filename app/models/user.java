package models;


import javax.persistence.*;

import com.avaje.ebean.Query;
import controllers.paramSearch;
import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.*;

import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 15.02.13
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */

//данные пользователей

@Entity
public class user extends Model {


      @Id
      @GeneratedValue(strategy = GenerationType.SEQUENCE)
      public Integer user_id;


    @Constraints.Required
      public String user_name;

      @Constraints.Required
      public String user_pass;
    @Constraints.Required
      public String user_fio;

      @ManyToOne
      @JoinColumn(name="dolg_id",referencedColumnName="dolg_id")
      public dolg dolg_u;

      @ManyToOne
      @JoinColumn(name="status_id",referencedColumnName="status_id")
      public status status_u;

      @Constraints.Required
      @Constraints.Email
      public String email;

      public String phone;

      public char is_Active;


    /**
     * Generic query helper for entity Computer with id Int
     */
    public static Finder<Integer,user> find = new Finder<Integer,user>(Integer.class, user.class);

    public static List<user> all (){
        return find.all();
    }

    public static Page<user> page(int page) {
        return
                find.where()
                        .findPagingList(15)
                        .getPage(page);
    }

    public static Page<user> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("user_fio", "%"+newsearch.param+"%")
                        .findPagingList(15)
                        .getPage(page);
    }



}
